---
title: "Images"
date: 2022-03-01T15:02:09+02:00
---

Here is an example gallery:

{{< gallery
    match="*"
    showExif="true"
    sortOrder="desc"
    loadJQuery="true"
    embedPreview="true"
    previewType="blur"
    rowHeight="150"
    resizeOptions="600x600 q90 Lanczos"
    thumbnailHoverEffect="enlarge"
>}}
